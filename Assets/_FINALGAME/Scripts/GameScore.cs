﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameScore : MonoBehaviour {

    Text myText;
    int score;
    int reaperValue = 230;

	// Use this for initialization
	void Start () {
        myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddScore()
    {
        score += reaperValue;
        myText.text = "Score: " + score.ToString();
    }
}
