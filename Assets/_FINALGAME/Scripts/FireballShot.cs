﻿using UnityEngine;
using System.Collections;

public class FireballShot : MonoBehaviour
{
    Animator myAnim;
    Rigidbody2D myBody;
    GameScore scoreScript;

    // Use this for initialization
    void Start()
    {
        myAnim = GetComponentInChildren<Animator>();
        myBody = GetComponentInParent<Rigidbody2D>();
        scoreScript = GameObject.FindWithTag("Score").GetComponent<GameScore>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velPos = new Vector2(transform.position.x, transform.position.y) + myBody.velocity.normalized;
        Vector3 upPos = new Vector3(velPos.x, velPos.y, 0);
        Vector3 forePos = new Vector3(transform.position.x, transform.position.y, -1);
        transform.LookAt(forePos, upPos);
    }

    void Smoulder()
    {
        myAnim.SetTrigger("Crash");
        GetComponentInChildren<Collider2D>().enabled = false;
        Destroy(gameObject, 2.0f);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            Smoulder();
        }

        if (collision.collider.tag == "Reaper")
        {
            Smoulder();
            scoreScript.AddScore();
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }
}
