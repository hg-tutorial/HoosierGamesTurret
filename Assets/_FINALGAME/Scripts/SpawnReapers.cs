﻿using UnityEngine;
using System.Collections;

public class SpawnReapers : MonoBehaviour {

    public GameObject reaper;
    public float spawnDelay = 2f;
    BoxCollider2D myColl;
    Transform reaperTray;
    public float reaperSpeed;

	// Use this for initialization
	void Start () {
        reaperTray = transform.GetChild(0);
        myColl = GetComponent<BoxCollider2D>();
        SpawnReaper();
	}
	
	// Update is called once per frame
	void Update () {
        reaperTray.position += Vector3.left * reaperSpeed * Time.deltaTime;
	}

    void SpawnReaper()
    {
        float xSpawn = Random.Range(-myColl.size.x/2, myColl.size.x/2);
        float ySpawn = Random.Range(-2.5f, 3f);

        Vector3 spawnPos = new Vector3(xSpawn, ySpawn, 0);

        GameObject newReaper = (GameObject)Instantiate(reaper, spawnPos, Quaternion.identity, reaperTray);


        StartCoroutine(DelaySpawn());
    }

    IEnumerator DelaySpawn()
    {
        yield return new WaitForSeconds(spawnDelay);
        SpawnReaper();
    }
}
