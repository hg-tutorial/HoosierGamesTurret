﻿using UnityEngine;
using System.Collections;

public class CannonShooter : MonoBehaviour {

    public GameObject fireBall;
    public Transform cannonTip;
    public float shotForce;
    public bool moveCannon;
    public float moveSpeed = 2;
    public Vector3 startPos;

	// Use this for initialization
	void Start () {
        startPos = transform.parent.position;

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos = new Vector3(mousePos.x, mousePos.y, 0);
        Vector3 forePos = new Vector3(transform.position.x, transform.position.y, -1);
        
        if (mousePos.x > transform.position.x)
        {
            transform.LookAt(forePos, mousePos - transform.position);
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            ShootFireBall();
        }

        if (moveCannon)
        {
            transform.parent.position = new Vector3(startPos.x + Mathf.Sin(Time.time * moveSpeed), transform.parent.position.y);
        }

	}

    void ShootFireBall()
    {
        GameObject ball = (GameObject)Instantiate(fireBall, cannonTip.position, transform.rotation);
        Rigidbody2D body = ball.GetComponent<Rigidbody2D>();
        body.AddForce(transform.up * shotForce, ForceMode2D.Impulse);
    }
}
